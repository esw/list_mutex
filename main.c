#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include "list.h"

#define MAX_ADDR_LEN 40
#define MAX_KEY_LEN 4
#define WRITERS_QUANTITY 1
#define WRITE_PERIOD_NS 20000
#define ITEM_QUANTITY 1000
#define PREDEF_ADDR_QUANTITY 4
#define TEST_DURATION 10

#define MIN(a,b) (((a)<(b))?(a):(b))

const char charset[] = "abcdefghijklmnopqrstuvwxyz";
const char predefined_addresses[][MAX_ADDR_LEN] = {
    "http://abcdefghijklmnopqrstuvwxyz.cz",
    "http://zyxwvutsrqponmlkjihgfedcba.cz",
    "http://aaaaaaaaaaaaazzzzzzzzzzzzz.cz",
    "http://zzzzzzzzzzzzzaaaaaaaaaaaaa.cz"};

esw_list_t list;
long reads;
long writes;

char used_keys[ITEM_QUANTITY][MAX_KEY_LEN+1];

pthread_t * readers;
pthread_t * writers;

void generate_random_key(char * key, int length){
    int i, r;
    for(i=0; i<length; i++){
        r = rand() % strlen(charset);
        key[i] = charset[r];
    }
    key[length]='\0';
}

void init_list(){
    int i;
    char key[MAX_KEY_LEN+1];
    int address_index;
    esw_list_init(&list);
    for(i=0; i<ITEM_QUANTITY; i++){
        generate_random_key(key, MAX_KEY_LEN);

        address_index = rand() % PREDEF_ADDR_QUANTITY;

        esw_list_push(&list, key, predefined_addresses[address_index]);

        strcpy(used_keys[i], key);
    }
}

void * reader_thread(void * arg){
    char key[MAX_KEY_LEN+1];
    char address[MAX_ADDR_LEN];

    long hits = 0;
    long miss = 0;
    long counter = 0;

    int check;
    int reference = 0;

    int i;

    for (i = 0; i < (int)(strlen(predefined_addresses[0])); ++i) {
        reference += (int)predefined_addresses[0][i];
    }

    while(1){
        generate_random_key(key, MAX_KEY_LEN);

        if(esw_list_find(&list, key, address, MAX_ADDR_LEN)==0){
            check = 0;
            for (i = 0; i < (int)(MIN(strlen(predefined_addresses[0]), strlen(address))); ++i) {
                check += (int)address[i];
            }
            assert(check == reference);
            hits++;
        }else{
            miss++;
        }

        if(counter%1000 == 0){
            __sync_fetch_and_add(&reads, 1);
        }
        counter++;
    }
}

void * writer_thread(void * arg){
    struct timespec tim;
    int64_t next_write;
    int64_t actual_time;

    long counter = 0;

    clock_gettime(CLOCK_MONOTONIC, &tim);
    actual_time = tim.tv_sec * 1000000000 + tim.tv_nsec;
    next_write = actual_time + WRITE_PERIOD_NS;

    while(1){
        do{
            clock_gettime(CLOCK_MONOTONIC, &tim);
            actual_time = tim.tv_sec * 1000000000 + tim.tv_nsec;
        }while(actual_time < next_write);
        next_write += WRITE_PERIOD_NS;

        esw_list_update(&list, used_keys[rand()%ITEM_QUANTITY], predefined_addresses[rand()%PREDEF_ADDR_QUANTITY]);

        if(counter%1000 == 0){
            __sync_fetch_and_add(&writes, 1);
        }
        counter++;
    }
}

int main(int argc, char *argv[])
{
    int i;
    long reads_local = 0;
    long writes_local = 0;
    long reads_local_last;
    long writes_local_last;

    int reader_quantity;

    if(argc != 2){
        printf("usage: ./list_mutex <reader-thread quantity>\n");
        exit(EXIT_FAILURE);
    }

    reader_quantity = atoi(argv[1]);

    if(reader_quantity < 1 || reader_quantity > 100){
        printf("min readers: 1, max readers: 100\n");
        exit(EXIT_FAILURE);
    }

    readers = (pthread_t*)calloc(sizeof(pthread_t),reader_quantity);
    writers = (pthread_t*)calloc(sizeof(pthread_t),WRITERS_QUANTITY);

    init_list();

    for(i=0; i<WRITERS_QUANTITY; i++){
        assert(pthread_create(&writers[i], NULL, writer_thread, NULL)==0);
    }

    for(i=0; i<reader_quantity; i++){
        assert(pthread_create(&readers[i], NULL, reader_thread, NULL)==0);
    }

    for(i=0; i<TEST_DURATION; i++){
        sleep(1);
        reads_local_last = reads_local;
        reads_local = __sync_fetch_and_add(&reads,0);
        printf("Reads: %ld k\n", reads_local - reads_local_last);
        writes_local_last = writes_local;
        writes_local = __sync_fetch_and_add(&writes,0);
        printf("Writes: %ld k\n", writes_local - writes_local_last);
        fflush(stdout);
    }

    esw_list_free_content(&list);

    return EXIT_SUCCESS;
}
