CC = gcc
CFLAGS = -static -O3 -Wall
#INCLUDES = -I../../lib/include
INCLUDES = 
#LFLAGS = -L../../lib/lib
LFLAGS = 
#LIBS = -lurcu -lpthread -lrt
LIBS = -lpthread -lrt

SRCS = main.c list.c

MAIN = list_mutex

$(MAIN): *.c *.h
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(SRCS) $(LFLAGS) $(LIBS)

clean:
	rm $(MAIN)
