#include "list.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

void esw_list_init(esw_list_t * list){
    assert(pthread_mutex_init(&list->lock, NULL) == 0);
    list->head = NULL;
}

void esw_list_push(esw_list_t * list, const char * const key, const char * const address){
    esw_node_t * node;
    assert(list != NULL);
    assert(key != NULL);
    assert(address != NULL);

    node = esw_list_create_node(key, address);

    pthread_mutex_lock(&list->lock);
    node->next = list->head;
    list->head = node;
    pthread_mutex_unlock(&list->lock);
}

void esw_list_append(esw_list_t * list, const char * const key, const char * const address){
    esw_node_t * current;
    esw_node_t * node;
    assert(list != NULL);
    assert(key != NULL);
    assert(address != NULL);

    node = esw_list_create_node(key, address);

    pthread_mutex_lock(&list->lock);

    current = list->head;
    if(current != NULL){
        while(current->next != NULL){
            current = current->next;
        }
        current->next = node;
    }else{
        list->head = node;
    }
    node->next = NULL;
    pthread_mutex_unlock(&list->lock);
}

void esw_list_remove(esw_list_t * list, const char * const key){
    esw_node_t * current;
    esw_node_t * next;
    assert(list != NULL);
    assert(key != NULL);
    pthread_mutex_lock(&list->lock);
    current = list->head;
    /* No node in list */
    if(current == NULL){
        pthread_mutex_unlock(&list->lock);
        return;
    }
    next = current->next;
    if(strcmp(current->key, key) == 0){
        /* Delete first node */
        list->head = current->next;
        esw_list_free_node(current);
    }else{
        /* Delete node in list */
        while(next != NULL){
            if(strcmp(next->key, key) == 0){
                current->next = next->next;
                esw_list_free_node(next);
                break;
            }
            current = next;
            next = current->next;
        }
    }
    pthread_mutex_unlock(&list->lock);
}

void esw_list_update(esw_list_t * list, const char * const key, const char * const address){
    esw_node_t * current;
    char * node_address;
    assert(list != NULL);

    pthread_mutex_lock(&list->lock);
    /* Replaces first occurrence in the list */
    current = list->head;
    while(current != NULL){
        if(strcmp(current->key, key) == 0){
            free(current->address);
            node_address = (char*)calloc(sizeof(char), strlen(address)+1);
            strcpy(node_address, address);
            current->address = node_address;
            break;
        }
        current = current->next;
    }
    pthread_mutex_unlock(&list->lock);
}

int esw_list_find(esw_list_t * list, const char * const key, char * address, const int max_len){
    esw_node_t * current;
    assert(list != NULL);
    assert(key != NULL);

    pthread_mutex_lock(&list->lock);
    current = list->head;
    while(current != NULL){
        if(strcmp(current->key, key) == 0){
            if(strlen(current->address) < max_len){
                strcpy(address, current->address);
            }else{
                strncpy(address, current->address, max_len-1);
                address[max_len-1]='\0';
            }
            pthread_mutex_unlock(&list->lock);
            return 0;
        }
        current = current->next;
    }
    pthread_mutex_unlock(&list->lock);
    return 1;
}

esw_node_t * esw_list_create_node(const char * const key, const char * const address){
    esw_node_t * node;
    char * node_key;
    char * node_address;

    node = (esw_node_t*)calloc(sizeof(esw_node_t),1);

    node_key = (char*)calloc(sizeof(char), strlen(key)+1);
    strcpy(node_key, key);
    node->key = node_key;

    node_address = (char*)calloc(sizeof(char), strlen(address)+1);
    strcpy(node_address, address);
    node->address = node_address;

    return node;
}

void esw_list_free_node(esw_node_t * node){
    free(node->key);
    free(node->address);
    free(node);
}

void esw_list_free_content(esw_list_t * list){
    esw_node_t * current;
    esw_node_t * tmp;
    assert(list != NULL);
    current = list->head;
    while (current) {
        tmp = current;
        current = current->next;
        esw_list_free_node(tmp);

    }
}

void esw_list_free(esw_list_t * list){
    esw_list_free_content(list);
    free(list);
}

void esw_list_print(esw_list_t * list){
    esw_node_t * node;
    assert(list != NULL);
    node = list->head;
    printf("List contains:\n");
    while(node != NULL){
        esw_list_node_print(node);
        node = node->next;
    }
}

void esw_list_node_print(esw_node_t * node){
    assert(node != NULL);
    printf("Key: %s\nAddress: %s\n", node->key, node->address);
}
